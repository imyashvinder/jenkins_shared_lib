def call(url, creds, nodelabel) {
    node(nodelabel) {
        stage("Clone the Repo for Performing CI/CD") {
            git credentialsId: "${creds}", url: "${url}"
        }
        if(params.clean) {
            cleanInstall()
        }
        if(params.build) {
            buildCode()
        }
        if(params.test) {
            testCodeStability()
        }
        if(params.test) {
            testCodeCoverage()
            publishCodeCoverage()
        }
        if(params.test) {
            testCodeQuality()
            publishCodeQuality()
        }
    }
}

def cleanInstall() {
    stage("Cleaning the Phase") {
        sh "mvn clean install"
    }
}

def buildCode() {
    stage("Building the Code") {
        sh "mvn compile"
    }
}

def testCodeStability() {
    stage("Testing Code Stability") {
        sh "mvn clean compile"
        publishCodeStability()
    }
}

def testCodeCoverage() {
    stage("Testing Code Coverage") {
        sh "mvn cobertura:cobertura"
    }
}

def testCodeQuality() {
    stage("Testing Code Quality") {
        sh "mvn checkstyle:checkstyle"
    }
}

def publishCodeCoverage() {
    cobertura autoUpdateHealth: false, autoUpdateStability: false, coberturaReportFile: 'target/site/cobertura/coverage.xml', conditionalCoverageTargets: '70, 0, 0', failNoReports: false, failUnhealthy: false, failUnstable: false, lineCoverageTargets: '80, 0, 0', maxNumberOfBuilds: 0, methodCoverageTargets: '80, 0, 0', onlyStable: false, sourceEncoding: 'ASCII', zoomCoverageChart: false
}

def publishCodeQuality() {
    checkstyle canComputeNew: false, defaultEncoding: '', healthy: '', pattern: 'target/checkstyle-result.xml', unHealthy: ''
}

def publishCodeStability() {
    findbugs canComputeNew: false, defaultEncoding: '', excludePattern: '', healthy: '', includePattern: '', isRankActivated: true, pattern: 'target/findbugs/findbugsXml.xml', unHealthy: ''
}


def echo() {
  node('master') {
    stage("echo") {
        sh "echo yashvinder"
    }
  }
}
