def clone_repo(nodelabel) {
    stage("clone repo") {
        node(nodelabel) {
            git credentialsId: "${creds}", url: "${url}"
        }
    }
}

def checkout_scm(nodelabel) {
    stage("checkout scm") {
        node(nodelabel) {
            checkout scm
        }
    }
}

def mvn_test(nodelabel) {
    stage("maven test") {
        node(nodelabel) {
            sh  "mvn test"
        }
    }
}

def mvn_compile(nodelabel) {
    stage("maven compile") {
        node(nodelabel) {
            sh "mvn compile"
        }
    }
}

def mvn_package(nodelabel) {
    stage("maven package") {
        node(nodelabel) {
          sh "mvn clean package"
        }   
    }
}


def cleanws() {
  post { 
    always { 
        cleanWs(
                cleanWhenAborted: true,
                cleanWhenFailure: true,
                cleanWhenNotBuilt: true,
                cleanWhenSuccess: true,
                cleanWhenUnstable: true,
                deleteDirs: true
            )
        }
    }
}
